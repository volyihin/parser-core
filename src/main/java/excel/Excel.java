package excel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import entity.Procurement;
import entity.Product;

/**
 * Представление excel файла, заточенного на сохранение закупок
 *
 * @author dima
 *
 */
public class Excel {

    private static final Logger logger = Logger.getLogger(Excel.class);

    private int rowNum = 0;
    private String path;
    private FileOutputStream fileOutputStream;
    private SXSSFWorkbook wb;
    private Sheet sheet;
    private Row currentRow;
    private Properties properties;

    public Excel(String path) throws FileNotFoundException {
        super();
        this.path = path;

    }

    /**
     * Пишет закупки в файл
     *
     * @param procurements
     * @throws FileNotFoundException
     */
    public void writeToFile(List<Procurement> procurements)
            throws FileNotFoundException {
        wb = new SXSSFWorkbook();
        sheet = wb.createSheet();

        currentRow = sheet.createRow(rowNum);
        writeHeader(currentRow);
        rowNum++;
        int i = 1;
        for (Procurement p : procurements) {
            saveProcurement(p,i);
            logger.debug("Сохраняем " + i + " закупку");
            i++;
        }

        try {
            fileOutputStream = new FileOutputStream(path);
        } catch (FileNotFoundException fnfe) {
            throw new FileNotFoundException("Файл " + path + " не найден");
        }

        try {
            wb.write(fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            logger.warn(e);
        } finally {
            wb.dispose();
        }

        logger.info("Наименование созданного файла " + path);
        logger.info("Сохранено " + procurements.size() + " закупок");
    }

    private void writeHeader(Row row) {

        properties = new Properties();

        try (InputStream is = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("header.properties")) {
            properties.load(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int cellnum = 0; cellnum < properties.size(); cellnum++) {
            Cell cell = row.createCell(cellnum);
            String value = properties.getProperty(Integer.toString(cellnum));
            cell.setCellValue(value);
        }

    }

    private void saveProcurement(Procurement procurement,int i) {
        currentRow = sheet.createRow(rowNum);
        List<Product> products = procurement.getProducts();

        if ((products != null) && (products.size() > 0)) {
            int j = 1;
            boolean saved = false;
            for (Product product : products) {
                writeToSheet(procurement, product, i);
                rowNum++;
                logger.debug("Продукт №" + j);
                j++;
                saved = true;
            }

            if (!saved)
                logger.debug("Пропустили закупку " + procurement);
        } else {
            writeToSheet(procurement, null ,i);
            rowNum++;
        }
    }

    private void writeToSheet(Procurement proc, Product product,int i) {
        currentRow = sheet.createRow(rowNum);
        writeProcutement(currentRow, proc, product, i);
    }

    private String getAttributeValue(Procurement proc, Product product,
                                     int cellnum, int i) {
        if (cellnum == 0)
            return proc.getDate();
        if (cellnum == 1)
            return Integer.toString(i);
        if ((cellnum == 2) && (proc.getPostAddress() != null))
            return proc.getPostAddress().toString();
        if (cellnum == 3)
            return proc.getCustomer();
        if (cellnum == 4)
            return proc.getInn();
        if (cellnum == 5)
            return proc.getOrderNumber();
        if (cellnum == 6)
            return proc.getOrderName();
        if (cellnum == 7)
            return proc.getStartPrice();
        if (cellnum == 8)
            return proc.getEndPrice();
        if ((cellnum == 9) & (product != null))
            return proc.getProvider();
        if ((cellnum == 10) & (product != null))
            return proc.getProviderInn();
        if ((cellnum == 11) & (product != null))
            return product.getName();
        if ((cellnum == 12) & (product != null))
            return product.getPrice();
        if ((cellnum == 13) & (product != null))
            return product.getAmount();
        if ((cellnum == 14) & (product != null))
            return product.getUnit();
        if (cellnum == 15)
            return proc.getUrl();
        if (cellnum == 16)
            return proc.getPageUrl();
        return "";
    }

    private void writeProcutement(Row row, Procurement proc, Product product, int i) {
        for (int cellnum = 0; cellnum <= properties.size(); cellnum++) {
            Cell cell = row.createCell(cellnum);

            String value = getAttributeValue(proc, product, cellnum, i);
            cell.setCellValue(value);

        }
    }

}
