
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import entity.Procurement;
import entity.dao.ProcurementDaoImpl;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;


import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import org.springframework.beans.factory.annotation.Autowired;
import web.ResultServlet;

import javax.servlet.Servlet;

public class JettyServer {

    private static final String CONTEXT_PATH = "/";
    private static final String MAPPING_URL = "*.jsp";

    public static void main(String[] args) throws Exception {
        System.setProperty("org.apache.jasper.compiler.disablejsr199", "false");
        Server server = new Server(8080);
        ServletContextHandler webAppContext = getServletContextHandler(getContext());
        server.setHandler(webAppContext);

        startHsqldb();

        server.start();

        startBrowser();

        server.join();

    }

    /**
     * Стартуем hsqldb
     */
    private static void startHsqldb() {
        org.hsqldb.Server hsqlServer = new org.hsqldb.Server();

        hsqlServer.setLogWriter(null);
        hsqlServer.setSilent(true);

        hsqlServer.setDatabaseName(0, "testdb");
        hsqlServer.setDatabasePath(0, "file:testdb");

        // Start the database!
        hsqlServer.start();
    }

    private static ServletContextHandler getServletContextHandler(WebApplicationContext context) throws IOException {
        ServletContextHandler contextHandler = new ServletContextHandler();
        contextHandler.setErrorHandler(null);
        contextHandler.setContextPath(CONTEXT_PATH);
        Servlet dispatcherServlet = new DispatcherServlet(context);
        ServletHolder dispatcherServletHolder = new ServletHolder(dispatcherServlet);
        contextHandler.addServlet(dispatcherServletHolder, MAPPING_URL);

        contextHandler.setWelcomeFiles(new String[]{ "WEB-INF/index.jsp"});

        contextHandler.addServlet(ResultServlet.class, "/resultservlet");

        contextHandler.addEventListener(new ContextLoaderListener(context));
        String resourceBase = new ClassPathResource("WEB-INF").getURI().toString();
        System.out.println(resourceBase);
        contextHandler.setResourceBase(resourceBase);
        return contextHandler;
    }

    private static WebApplicationContext getContext() {
        XmlWebApplicationContext context = new XmlWebApplicationContext();
        context.setConfigLocation("classpath*:applicationContext.xml");
        return context;
    }

    private static void startBrowser() {
        String url = "http://localhost:8080";

        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(url));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("xdg-open " + url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
