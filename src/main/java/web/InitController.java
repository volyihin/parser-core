package web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by volyihin on 9/2/14.
 */
@Controller
public class InitController {

    @RequestMapping(value = "/")
    public String init(Model model) {
        return "index";
    }
}
