package web;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Strings;

import core.Parser;

public class ResultServlet extends HttpServlet {

	Parser parser;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		/*
		if (!parser.isError()) {
			response.setIntHeader("Refresh", 10);
		}
		
		*/
		request.setAttribute("procurements", parser.getProcurements());
		request.setAttribute("all", parser.getCount());
		request.setAttribute("current", parser.getCurrent());
		request.setAttribute("date",
				new SimpleDateFormat("HH:mm:ss").format(new Date()));
		response.setContentType("text/plain; charset=windows-1251");

		request.setAttribute("message", parser.getMessage());

		// System.out.println(request.getParameterNames());
		// System.out.println("\n\n\n\n\n\nmessage\n\n\n" + message);

		request.getRequestDispatcher("resultservlet/some-result.jsp").forward(
				request, response);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String url = new String(request.getParameter("url").getBytes(),
				"cp1251");
		String start = request.getParameter("start");
		String end = request.getParameter("end");
		if (!Strings.isNullOrEmpty(url)
				|| (!Strings.isNullOrEmpty(start) || (!Strings
						.isNullOrEmpty(end)))) {
			int s = Integer.valueOf(start);
			int e = Integer.valueOf(end);
			response.sendRedirect("resultservlet");
			parser = new Parser(url, s, e);
			parser.disableInputParam();
			parser.start();
		} else {
			response.sendRedirect("/");
		}

	}
}