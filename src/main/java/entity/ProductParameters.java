package entity;

/**


 *
 * @author dima
 * 
 */
public enum ProductParameters {
	LAST_CONTRACT_SUM,
	PRODUCER,
	PRODUCER_INN,
	PRODUCT,
	PRICE,
	COUNT,
	UNIT;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
