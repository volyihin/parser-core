package entity.dao;

import entity.Procurement;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by d.volyhin on 28.08.2014.
 */
@Transactional
public class ProcurementDaoImpl {

    @PersistenceContext
    private EntityManager em;

    public String save(Procurement p) {
        em.persist(p);
        return p.getId();
    }

    public List<Procurement> getAll() {
        return em.createQuery("SELECT p FROM Procurement p", Procurement.class).getResultList();
    }
}
