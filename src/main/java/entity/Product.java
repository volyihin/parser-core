package entity;

import javax.persistence.*;

@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

	/**
	 * Наименование
	 */
	private String name;
	/**
	 * Код по ОКПД
	 */
	private String code;
	/**
	 * Единица измерения
	 */
	private String unit;
	/**
	 * Цена за ед.изм.
	 */
	private String price;

    public String getAmount() {
        return amount;
    }

    /**
	 * Количество
	 */
	private String amount;
	/**
	 * Стоимость
	 */
	private String summa;

    @ManyToOne
    private Procurement procurement;

	public Product() {
		super();
	}

	public Product(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getSumma() {
		return summa;
	}

	public void setSumma(String summa) {
		this.summa = summa;
	}

	@Override
	public String toString() {
		return "\nProduct [name=" + name + ", code=" + code + ", unit=" + unit + ", price=" + price
				+ ", count=" + amount + ", summa=" + summa + "]";
	}

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
