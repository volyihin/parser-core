package entity;

import java.util.List;


public class Unloading {
	private String user;
	private List<Procurement> procurements;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public List<Procurement> getProcurements() {
		return procurements;
	}
	public void setProcurements(List<Procurement> procurements) {
		this.procurements = procurements;
	}
	

}
