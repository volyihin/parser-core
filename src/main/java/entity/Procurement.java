package entity;

import java.util.List;
import java.util.Map;

import core.url.UrlConstructer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Procurement {

	@Override
	public String toString() {
		return "P  [заказчик=" + customer + ", дата=" + date + ", конечная цена=" + endPrice
				+ ", была ли ошибка=" + error + ", фо=" + fo + ", ид=" + id + ", инн="
				+ inn + ", наименование заказа=" + orderName + ", номер заказа=" + orderNumber
				+ ", адресс=" + postAddress + ", товары=" + products + ", поставщик="
				+ provider + ", регион=" + region + ", начальная цена=" + startPrice
				+ ", ссылка=" + url + "]";
	}

    @Id
	private String id;
	private String date;
	private String fo;
    @OneToOne
	private PostAddress postAddress;
	private String customer;
	private String orderNumber;
	private String orderName;
	private String startPrice;
	private String endPrice;
	private String provider;
    @OneToMany
	private List<Product> products;
	private String region;
	private String url;
	private String inn;
	private String providerInn;
	private String pageUrl;

	private boolean error;

	public PostAddress getPostAddress() {
		return postAddress;
	}

	public void setPostAddress(PostAddress postAddress) {
		this.postAddress = postAddress;
	}

	public Procurement() {
		super();
	}

	public Procurement(String noticeId, String url) {
		this.id = noticeId;
		this.url = url;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFo() {
		return fo;
	}

	public void setFo(String fo) {
		this.fo = fo;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(String startPrice) {
		this.startPrice = startPrice;
	}

	public String getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(String endPrice) {
		this.endPrice = endPrice;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

	public String getInn() {
		return inn;
	}

	public String getProviderInn() {
		return providerInn;
	}

	public void setProviderInn(String providerInn) {
		this.providerInn = providerInn;
	}

	public static Procurement fromUrl(String url) {
		Map<String, String> map = UrlConstructer.getQueryMap(url);

		String noticeId = map.get("noticeId");
		String notificationId = map.get("notificationId");
		String regNumber = map.get("regNumber");
		String lotId = map.get("lotId");		

		if (noticeId != null)
			return new Procurement(noticeId, url);

		if (notificationId != null)
			return new Procurement(notificationId, url);

		if (regNumber != null)
			return new Procurement(regNumber, url);
		
		if (lotId != null)
			return new Procurement(lotId, url);
		
		

		return new Procurement();
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public static String orderNumberToString(List<Procurement> procuremnts) {
		StringBuilder sb = new StringBuilder("\n");

		for (Procurement pr : procuremnts) {
			sb.append(pr.getOrderNumber()).append("\n");
		}

		return sb.toString();
	}
}
