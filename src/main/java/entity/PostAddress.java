package entity;

import com.google.common.base.Preconditions;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
public class PostAddress {

	public PostAddress(String address) {
		this.address = address;
	}

    @Id
    @GeneratedValue
    private Long id;

	@Override
	public String toString() {
		return address;
	}

	private String address;;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static PostAddress fromString(String address) {
		Preconditions.checkNotNull(address);
		PostAddress postAddress = new PostAddress(address);
		return postAddress;
	}

}
