package core.handler;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BaseHandler implements Handler {

	private static final Logger logger = Logger.getLogger(BaseHandler.class);

	public BaseHandler(WebElement element) {
		super();
		this.element = element;
	}

	private WebElement element;
	private HashMap<String, String> values = new HashMap<String, String>();

	public void execute() {
		List<WebElement> tdElements = getElement().findElements(
				By.tagName("td"));
		if (tdElements.size() == 2) {
			String key = tdElements.get(0).getText();
			String value = tdElements.get(1).getText();
			logger.debug(key + "=" + value);
			values.put(key, value);
		} else {
			if ("orderInfoHdr".equals(element.getAttribute("class")))
				logger.debug("\n\n" + element.getText().toUpperCase() + "\n\n");
		}
	}

	public WebElement getElement() {
		return element;
	}

	public void setElement(WebElement element) {
		this.element = element;
	}

	public HashMap<String, String> getValues() {
		return values;
	}

	public void setValues(HashMap<String, String> values) {
		this.values = values;
	}

}