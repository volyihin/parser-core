package core.handler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;

/**
 * Класс для работы с запросами, инкапсулирует получение следующего запроса Не
 * имеет состояния.
 * <ul>
 * <li>Инкрементирует парметры страницы</li>
 * <li>Находит необходимы параметр</li>
 * </ul>
 * 
 * @author ноут
 * 
 */
public class UrlEncoder {

	private static Pattern PAGE_NO_PATTERN = Pattern.compile("pageNo=\\d*");
	private static Pattern INDEX_PATTERN = Pattern.compile("index=\\d*");
	private static Pattern EXT_PATTERN = Pattern.compile("ext=\\w*");

	final private static Logger logger = Logger.getLogger(UrlEncoder.class);

	final private static ScriptEngine engine;

	private static final String PAGE_NO = "pageNo";

	private static final String INDEX = "index";

	public static String nextUrl(String url) {
		if (Strings.isNullOrEmpty(url))
			throw new IllegalStateException("Передан пустой параметр");
		if (url.contains(PAGE_NO))
			return nextNewUrl(url);

		if (url.contains(INDEX))
			return nextOldUrl(url);

		return "";

	}

	/**
	 * Возвращает ссылку для старой формы
	 * 
	 * @param url
	 * @return
	 */
	private static String nextOldUrl(String url) {

		url = nextIndex(url);
		url = nextExt(url);

		return url;
	}

	/**
	 * Возвращает ссылку с приготовленным ext
	 * 
	 * @param url
	 * @return
	 */
	private static String nextExt(String url) {
		String query = url.substring(url.indexOf('?') + 1, url.length());
		query = setParameter("ext", "", query);

		String extResult = ext(query);

		Matcher m = EXT_PATTERN.matcher(url);
		String keyValue = "";
		if (m.find())
			keyValue = m.group();
		else
			throw new IllegalStateException("Не нашли параметр ext в url=" + url);

		return url.replace(keyValue, "ext=" + extResult);

	}

	/**
	 * Считает хэш для строки параметров
	 * 
	 * @param query
	 * @return
	 */
	public static String ext(String query) {
		try {
			return (String) engine.eval("kall('" + query + "')");
		} catch (Exception e) {
			logger.warn("Парметр ext е смогли обновить ");
		}
		return getParameter("ext", query);
	}

	/**
	 * Возвращает ссылку с index++
	 * 
	 * @param url
	 * @return
	 */
	private static String nextIndex(String url) {
		return nextPageNumber(url, INDEX_PATTERN, INDEX);
	}

	/**
	 * Возвращает ссылку для новой формы
	 * 
	 * @param url
	 * @return
	 */
	private static String nextNewUrl(String url) {
		return nextPageNo(url);
	}

	private static String nextPageNo(String url) {
		return nextPageNumber(url, PAGE_NO_PATTERN, PAGE_NO);
	}

	/**
	 * Находит параметр по патерну и инекрементирует его
	 * 
	 * @param url
	 * @param searchPattern
	 * @param pageParameter
	 * @return
	 */
	private static String nextPageNumber(String url, Pattern searchPattern, String pageParameter) {
		Matcher m = searchPattern.matcher(url);
		String keyValue = "";
		if (m.find())
			keyValue = m.group();
		else {
			throw new IllegalStateException("Не нашли параметр " + pageParameter + " в url=" + url);
		}
		String indexValue = "";
		if (keyValue.contains("=")) {
			indexValue = keyValue.split("=")[1];
		}

		int pageNumber = Integer.valueOf(indexValue);
		pageNumber++;
		logger.info("Индекс = " + (pageNumber - 1));
		return url.replace(keyValue, pageParameter + "=" + pageNumber);
	}

	/**
	 * Получить значение параметра
	 * 
	 * @param key
	 * @param query
	 * @return
	 */
	public static String getParameter(String key, String query) {
		String[] params = query.split("&");
		for (String param : params) {
			String[] p = param.split("=");

			if ((p.length == 2) && (p[0].equals(key)))
				return p[1];
		}

		return "";
	}

	/**
	 * 
	 */
	private static String setParameter(String key, String value, String query) {
		StringBuilder sb = new StringBuilder("");

		for (String keyValue : query.split("&")) {
			String[] kv = keyValue.split("=");
			String v = "";
			if (kv.length != 1)
				v = kv[1];
			String k = kv[0];
			if (key.equals(k))
				sb.append(key).append("=").append(value);
			else
				sb.append(k).append("=").append(v);
			sb.append("&");
		}

		logger.info("Начальная страница " + sb.toString());

		return sb.toString();

	}

	public static String setStartPage(int start, String url) {
		if (Strings.isNullOrEmpty(url))
			throw new IllegalStateException("Передан пустой параметр url");
		if (url.contains(PAGE_NO))
			return setParameter(PAGE_NO, String.valueOf(start), url);

		if (url.contains(INDEX))
			return setParameter(INDEX, String.valueOf(start), url);

		return url;
	}

	// @formatter:off
	private static String KALL_FUNCTION = "var kall = function (a) {"
			+ "    function b(a, d) {"
			+ "        var b, g, e, f, j;"
			+ "        e = a & 2147483648;"
			+ "        f = d & 2147483648;"
			+ "        b = a & 1073741824;"
			+ "        g = d & 1073741824;"
			+ "        j = (a & 1073741823) + (d & 1073741823);"
			+ "        return b & g ? j ^ 2147483648 ^ e ^ f : b | g ? j & 1073741824 ? j ^ 3221225472 ^ e ^ f : j ^ 1073741824 ^ e ^ f : j ^ e ^ f"
			+ "    }"
			+ ""
			+ "    function d(a, d, g, e, f, j, h) {"
			+ "		a = b(a, b(b(d & g | ~d & e, f), h));"
			+ "        return b(a << j | a >>> 32 - j, d)"
			+ "        		}"
			+ ""
			+ "    function e(a, d, g, e, f, j, h) {"
			+ "        a = b(a, b(b(d & e | g & ~e, f), h));"
			+ "        return b(a << j | a >>> 32 - j, d)"
			+ "    }"
			+ ""
			+ "    function f(a, d, g, e, f, j, h) {"
			+ "        a = b(a, b(b(d ^ g ^ e, f), h));"
			+ "        return b(a << j | a >>> 32 - j, d)"
			+ "    }"
			+ "    function h(a, d, g, e, f, j, h) {"
			+ ""
			+ "        a = b(a, b(b(g ^ (d | ~e),"
			+ "            f), h));"
			+ "        return b(a << j | a >>> 32 - j, d)"
			+ "    }"
			+ ""
			+ "    function g(a) {"
			+ "        var d = '',"
			+ "				b = '',"
			+ "				g;"
			+ "        for (g = 0; g <= 3; g++) b = a >>> g * 8 & 255, b = '0' + b.toString(16), d += b.substr(b.length - 2, 2);"
			+ "        return d"
			+ "    }"
			+ "    "
			+ "    var j = [],"
			+ "        k, n, l, p, m, o, q, r, a = function (a) {"
			+ "            for (var a = a.replace(/\r\n/g, '\n'), d = '', b = 0; b < a.length; b++) {"
			+ "                var g = a.charCodeAt(b);"
			+ "                g < 128 ? d += String.fromCharCode(g) : (g > 127 && g < 2048 ? d += String.fromCharCode(g >> 6 | 192) : (d += String.fromCharCode(g >> 12 | 224), d += String.fromCharCode(g >> 6 & 63 | 128)), d += String.fromCharCode(g & 63 | 128))"
			+ "            }"
			+ "            return d"
			+ "        }(a),"
			+ "        j = function (a) {"
			+ "            var d, b = a.length;"
			+ "            d ="
			+ "                b + 8;"
			+ "            for (var g = ((d - d % 64) / 64 + 1) * 16, e = Array(g - 1), f = 0, j = 0; j < b;) d = (j - j % 4) / 4, f = j % 4 * 8, e[d] |= a.charCodeAt(j) << f, j++;"
			+ "            e[(j - j % 4) / 4] |= 128 << j % 4 * 8;"
			+ "            e[g - 2] = b << 3;"
			+ "            e[g - 1] = b >>> 29;"
			+ "            return e"
			+ "        }(a);"
			+ "    m = 1732584193;"
			+ "    o = 4023233417;"
			+ "    q = 2562383102;"
			+ "    r = 271733878;"
			+ "	for (a = 0; a < j.length; a += 16) k = m, n = o, l = q, p = r, m = d(m, o, q, r, j[a + 0], 7, 3614090360), r = d(r, m, o, q, j[a + 1], 12, 3905402710), q = d(q, r, m, o, j[a + 2], 17, 606105819), o = d(o, q, r, m, j[a + 3], 22, 3250441966), m = d(m, o, q, r, j[a + 4], 7, 4118548399), r = d(r, m, o, q, j[a + 5], 12, 1200080426), q = d(q, r, m, o, j[a + 6], 17, 2821735955), o ="
			+ "        d(o, q, r, m, j[a + 7], 22, 4249261313), m = d(m, o, q, r, j[a + 8], 7, 1770035416), r = d(r, m, o, q, j[a + 9], 12, 2336552879), q = d(q, r, m, o, j[a + 10], 17, 4294925233), o = d(o, q, r, m, j[a + 11], 22, 2304563134), m = d(m, o, q, r, j[a + 12], 7, 1804603682), r = d(r, m, o, q, j[a + 13], 12, 4254626195), q = d(q, r, m, o, j[a + 14], 17, 2792965006), o = d(o, q, r, m, j[a + 15], 22, 1236535329), m = e(m, o, q, r, j[a + 1], 5, 4129170786), r = e(r, m, o, q, j[a + 6], 9, 3225465664), q = e(q, r, m, o, j[a + 11], 14, 643717713), o = e(o, q, r, m, j[a + 0], 20, 3921069994), m = e(m, o, q, r, j[a + 5], 5, 3593408605), r = e(r, m, o, q, j[a + 10], 9, 38016083),"
			+ "    q = e(q, r, m, o, j[a + 15], 14, 3634488961), o = e(o, q, r, m, j[a + 4], 20, 3889429448), m = e(m, o, q, r, j[a + 9], 5, 568446438), r = e(r, m, o, q, j[a + 14], 9, 3275163606), q = e(q, r, m, o, j[a + 3], 14, 4107603335), o = e(o, q, r, m, j[a + 8], 20, 1163531501), m = e(m, o, q, r, j[a + 13], 5, 2850285829), r = e(r, m, o, q, j[a + 2], 9, 4243563512), q = e(q, r, m, o, j[a + 7], 14, 1735328473), o = e(o, q, r, m, j[a + 12], 20, 2368359562), m = f(m, o, q, r, j[a + 5], 4, 4294588738), r = f(r, m, o, q, j[a + 8], 11, 2272392833), q = f(q, r, m, o, j[a + 11], 16, 1839030562), o = f(o, q, r, m, j[a + 14], 23, 4259657740), m = f(m, o, q, r, j[a + 1], 4, 2763975236),"
			+ "    r = f(r, m, o, q, j[a + 4], 11, 1272893353), q = f(q, r, m, o, j[a + 7], 16, 4139469664), o = f(o, q, r, m, j[a + 10], 23, 3200236656), m = f(m, o, q, r, j[a + 13], 4, 681279174), r = f(r, m, o, q, j[a + 0], 11, 3936430074), q = f(q, r, m, o, j[a + 3], 16, 3572445317), o = f(o, q, r, m, j[a + 6], 23, 76029189), m = f(m, o, q, r, j[a + 9], 4, 3654602809), r = f(r, m, o, q, j[a + 12], 11, 3873151461), q = f(q, r, m, o, j[a + 15], 16, 530742520), o = f(o, q, r, m, j[a + 2], 23, 3299628645), m = h(m, o, q, r, j[a + 0], 6, 4096336452), r = h(r, m, o, q, j[a + 7], 10, 1126891415), q = h(q, r, m, o, j[a + 14], 15, 2878612391), o = h(o, q, r, m, j[a + 5], 21, 4237533241),"
			+ "    m = h(m, o, q, r, j[a + 12], 6, 1700485571), r = h(r, m, o, q, j[a + 3], 10, 2399980690), q = h(q, r, m, o, j[a + 10], 15, 4293915773), o = h(o, q, r, m, j[a + 1], 21, 2240044497), m = h(m, o, q, r, j[a + 8], 6, 1873313359), r = h(r, m, o, q, j[a + 15], 10, 4264355552), q = h(q, r, m, o, j[a + 6], 15, 2734768916), o = h(o, q, r, m, j[a + 13], 21, 1309151649), m = h(m, o, q, r, j[a + 4], 6, 4149444226), r = h(r, m, o, q, j[a + 11], 10, 3174756917), q = h(q, r, m, o, j[a + 2], 15, 718787259), o = h(o, q, r, m, j[a + 9], 21, 3951481745), m = b(m, k), o = b(o, n), q = b(q, l), r = b(r, p);	return (g(m) + g(o) + g(q) + g(r)).toLowerCase()"
			+ "};";

	static {
		// create a script engine manager
		ScriptEngineManager factory = new ScriptEngineManager();
		// create a JavaScript engine
		engine = factory.getEngineByName("JavaScript");
		try {
			// evaluate JavaScript code from String
			engine.eval(KALL_FUNCTION);
		} catch (Exception e) {
			logger.warn("Невозможно загрузить функцтю kall");
		}
	}

}
// @formatter:on
