package core.handler;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.google.common.collect.Lists;

import entity.Product;

public class ProductHandler {

	private static final Logger logger = Logger.getLogger(ProductHandler.class);

	private List<Product> products = Lists.newArrayList();

	public ProductHandler(WebElement element) {
		super();
		this.element = element;
	}

	private WebElement element;

	public void execute() {

		List<WebElement> trElements = element.findElements(By.xpath("td/table/tbody/tr"));
		for (WebElement tr : trElements) {
			List<WebElement> tdElements = tr.findElements(By.xpath("td"));
			if (tdElements.size() == 6) {
				Product product = new Product();
				product.setName(tdElements.get(0).getText());
				product.setCode(tdElements.get(1).getText());
				product.setUnit(tdElements.get(2).getText());
				product.setPrice(tdElements.get(3).getText());
				product.setAmount(tdElements.get(4).getText());
				product.setSumma(tdElements.get(5).getText());
				logger.info(product.toString());
				products.add(product);
			}
		}
	}

	public WebElement getElement() {
		return element;
	}

	public void setElement(WebElement element) {
		this.element = element;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}