package core.handler;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ProviderHandler implements Handler {

	private static final Logger logger = Logger.getLogger(ProviderHandler.class);

	private WebElement element;
	private String provider;
	private String providerInn;

	public ProviderHandler(WebElement element) {
		this.element = element;
	}

	@Override
	public void execute() {
		List<WebElement> trElements = element.findElements(By.xpath("td/table/tbody/tr"));
		for (WebElement tr : trElements) {
			List<WebElement> tdElements = tr.findElements(By.xpath("td"));
			if (tdElements.size() == 8) {
				provider = tdElements.get(0).getText();
				providerInn = tdElements.get(5).getText();
				logger.info("provider=" + provider);
				logger.info("providerInn=" + providerInn);
			}
		}

	}

	public String getProvider() {
		return provider;
	}

	public String getProviderInn() {
		return providerInn;
	}

}
