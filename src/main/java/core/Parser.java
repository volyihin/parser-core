package core;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import entity.dao.ProcurementDaoImpl;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import core.page.common.CommonInfoFactory;
import core.page.common.ICommonInfo;
import core.page.iterator.IIterablePage;
import core.page.iterator.IterablePageResolver;
import entity.Procurement;
import excel.Excel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

public class Parser extends Thread implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

    @Autowired
    private ProcurementDaoImpl procurementDao;

	static {

		Path path = Paths.get("log");

		if (Files.notExists(path)) {
			try {
				Files.createDirectory(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.setProperty("current.date", DATE_FORMAT.format(new Date()));
	}

	public Parser(String url, int start, int end) {
		super();
		this.url = url;
		this.start = start;
		this.end = end;
	}

	public Parser() {
	}

	private static final Logger logger = Logger.getLogger(Parser.class);

	private static final String CTRL_C = "\n\nЕсли хотите остановить парсер и сохранить всю выгруженную информацию то нажмите 'ctrl+c' при выгрузке\n\n";
	private static final String SKIP_INPUT = "Если хотите выгрузить всю информацию, то вместо начальная  и конечная старница нажмите 'Enter'\n\n";

	private static final Integer DEFAULT_START_PAGE = 1;
	private static final Integer DEFAULT_END_PAGE = 5000;

	private String url;
	private ArrayList<Procurement> procurements = Lists.newArrayList();
	private int start;
	private int end;
	private int count;
	private int current = 0;
	private Excel excel;
	private volatile boolean saved = false;
	private boolean writeToFile = true;
	private boolean inputParam = true;
	private String message = "";
	private boolean error = false;
	int i = 0;

	private void init() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			public void run() {
				if (saved)
					return;
				logger.info("Непредвиденное завершение.");
				if (getProcurements().size() > 0) {
					logger.info("Сохраняем исследованые закупки. Количество " + getProcurements().size());
					saveFile(getProcurements());
				} else {
					logger.info("Ни одной закупки не было исследовано. Excel файл не будет создан.");
				}
			}
		}));

	}

	private void readParameters() {
		Scanner scanner = null;
		try {

			scanner = new Scanner(System.in);
			boolean isEntered = false;
			while (!isEntered) {

				System.out.println(CTRL_C + SKIP_INPUT);

				System.out.println("Введите ссылку:");
				String urlParam = scanner.nextLine();
				System.out.println("Введите начальную страницу:");
				start = getStartValue(scanner.nextLine());
				System.out.println("Введите конечную страницу:");
				end = getEndValue(scanner.nextLine());

				if (!Strings.isNullOrEmpty(urlParam)) {
					url = urlParam;
					isEntered = true;
				}

			}

		} catch (IllegalArgumentException iae) {
			logger.warn("Неправильно заданы параметры");  
			System.exit(0);
		} finally {
			if (scanner != null)
				scanner.close();
		}

	}

	private int getEndValue(String next) {
		Integer end = DEFAULT_END_PAGE;
		try {
			end = Integer.parseInt(next);
		} catch (Exception e) {
		}
		System.out.println("Выгрузка закупок закончится на " + end + " старнице");

		return end;
	}

	private int getStartValue(String next) {
		Integer start = DEFAULT_START_PAGE;
		try {
			start = Integer.parseInt(next);
		} catch (Exception e) {
		}
		System.out.println("Выгрузка закупок начнется на " + start + " старнице");

		return start;
	}

	private void saveFile(List<Procurement> procurements) {
		int end = procurements.size() / 10 + start;
		String path = procurements.size() + "__" + "_pages_" + start + "_to_" + end + "_"
				+ DATE_FORMAT.format(new Date()) + ".xlsx";
		try {
			excel = new Excel(path);
			excel.writeToFile(procurements);
		} catch (IOException e) {
			logger.warn("Ошибка записи в файл", e);
			message = message.concat("Ошибка записи в файл").concat("<br>");
		}
		
		message = message.concat("Наименование созданного файла " + path).concat("<br>");
		message = message.concat("Сохранено " + procurements.size() + " закупок").concat("<br>");
		
		
	}

	public static Integer getUniqs(List<Procurement> procurements) {
		Set<Long> set = Sets.newHashSet();
		for (Procurement p : procurements) {
			String orderNumber = "";
			try {
				orderNumber = p.getOrderNumber();
				if (!Strings.isNullOrEmpty(orderNumber))
					set.add(Long.valueOf(orderNumber));
			} catch (Exception e) {
				logger.warn("Не могу прибавить " + orderNumber + " к уникальным");
			}
		}

		return set.size();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void disableWriteToFile() {
		this.writeToFile = false;
	}

	public void disableInputParam() {
		this.inputParam = false;
	}

	public List<Procurement> getProcurements() {
		return procurements;
	}

	@Override
	public void run() {

        Procurement p  = new Procurement();
        p.setCustomer("dqwdqwd");
        System.out.println(p);

        procurementDao.save(p);

        System.out.println(procurementDao.getAll());

		init();

		if (inputParam)
			readParameters();

		logger.info("Исходная ссылка: " + url);
		IIterablePage ip = null;

		try {

			try {
				ip = IterablePageResolver.fromUrl(url, start);
				ip.setEnd(end);
			} catch (SocketTimeoutException ste) {
				message.concat("Не удалось получить страницу:" + url);
				logger.error("Не удалось получить страницу:" + url);
			} catch (Exception e1) {
				logger.error("Неправильно сформированная ссылка", e1);
				message.concat("Неправильно сформированная ссылка:" + url);
				error = true;
				System.exit(-1);
			}
			setCount(ip.getCount());
			logger.info("ДОЛЖНО БЫТЬ=" + getCount());
			while (ip.hasNext()) {

				List<Procurement> pageProcurements = ip.next();
				logger.info("Номера заказов");

				for (Procurement pr : pageProcurements) {
					logger.info("Исследуется " + current++ + " закупка");
					ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(pr);
					commonInfo.start();
					pr = commonInfo.getProcurement();
					logger.info("Полученная инфомация о закупке\n" + pr.toString());
				}

				logger.info(Procurement.orderNumberToString(pageProcurements));
				getProcurements().addAll(pageProcurements);

				logger.info("Всего " + getProcurements().size() + " закупок");
				message.concat("Всего " + getProcurements().size() + " закупок");
				ip.nextUrl();
			}

		} catch (Exception e) {
			logger.warn("Произошла ошибка", e);
			message = message.concat("Произошла ошибка:<br>" + e.getMessage()).concat("<br>");
			error = true;
		} finally {
			logger.info("ВСЕГО=" + getProcurements().size());
			if (writeToFile)
				saveFile(getProcurements());
			saved = true;
			message = message.concat("Выгружено " + getProcurements().size() + " закупок.");
		}
	}

	public String getMessage() {
		return message;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

}
