package core.page.common;

import entity.Procurement;

public interface ICommonInfo {

	public abstract void start();

	public abstract Procurement getProcurement();

}