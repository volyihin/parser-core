package core.page.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import core.handler.BaseHandler;
import entity.PostAddress;
import entity.Procurement;
import entity.Product;

public class CommonInfo44 implements ICommonInfo {
	private final static Logger logger = Logger.getLogger(CommonInfo44.class);

	private Procurement procurement;
	private List<Product> products = Lists.newArrayList();
	private WebDriver page;
	private boolean productExist = false;
	private Map<String, String> values = new HashMap<String, String>();

	public CommonInfo44(Procurement p) {
		super();
		this.procurement = p;
	}

	@Override
	public void start() {
		page = new HtmlUnitDriver();
		page.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
		
		logger.info("Обрабатывается  закупка с URL " + procurement.getUrl());

		try {

			page.get(procurement.getUrl());

			List<WebElement> tables = page.findElements(By
					.xpath("/html/body/div/div/div/div[3]/div/div/table"));

			for (WebElement table : tables) {
				List<WebElement> trs = table.findElements(By.tagName("tr"));

				for (WebElement tr : trs) {
					BaseHandler bh = new BaseHandler(tr);
					bh.execute();
					values.putAll(bh.getValues());
				}

			}

			try {
				List<WebElement> trs = page
						.findElements(By
								.xpath("//h2[contains(text(),'Информация об объекте закупки')]/following-sibling::table[1]/tbody/tr"));
				for (WebElement tr : trs) {
					List<WebElement> tds = tr.findElements(By.tagName("td"));
					Product product = null;
					if (tds.size() == 6) {
						product = new Product();
						product.setName(tds.get(0).getText());
						product.setCode(tds.get(1).getText());
						product.setUnit(tds.get(2).getText());
						product.setAmount(tds.get(3).getText());
						product.setPrice(tds.get(4).getText());
						product.setSumma(tds.get(4).getText());
						products.add(product);
					}

				}
			} catch (NoSuchElementException e) {
				logger.warn("Текущая страница не содержит вкладки 'Информация о контракте'"
						+ "xpath=");
			}

			String date = "";
			List<WebElement> dates = page.findElements(By
					.xpath("//div[contains(text(),'Опубликовано')]"));
			if (dates.size() == 1)
				date = dates.get(0).getText();

			if (!Strings.isNullOrEmpty(date))
				procurement.setDate(date.replace("Опубликовано", ""));

			fillProcuremt();

		} catch (Exception e) {
			logger.warn("Ошибка обработки закупки", e);
		}
	}

	public void fillProcuremt() {
		String customer = values.get("Заказчик");

		if (Strings.isNullOrEmpty(customer))
			customer = values.get("Закупку осуществляет");

		procurement.setCustomer(customer);

		String innKpp = Objects.firstNonNull(values.get("ИНН \\ КПП"), "");
		String[] inn = innKpp.split("\\\\");
		procurement.setInn(inn[0]);
		if (procurement.getOrderNumber() == null)
			procurement.setOrderNumber(values.get("Номер извещения"));

		if (Strings.isNullOrEmpty(procurement.getOrderName())) {
			String orderName = values.get("Наименование закупки");
			if (Strings.isNullOrEmpty(orderName)) {
				orderName = values.get("Наименование объекта закупки");

			}
			procurement.setOrderName(orderName);
		}

		String address = values.get("Адрес места нахождения");

		if (Strings.isNullOrEmpty(address))
			address = values.get("Место нахождения");

		procurement.setProducts(products);

		if (address != null)
			procurement.setPostAddress(PostAddress.fromString(address));
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return Joiner.on("\n").withKeyValueSeparator("=").join(values);
	}

	public boolean isProductExist() {
		return productExist;
	}

	public void setProductExist(boolean productExist) {
		this.productExist = productExist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.page.ICommonInfo#getProcurement()
	 */
	@Override
	public Procurement getProcurement() {
		return procurement;
	}

	public void setProcurement(Procurement procurement) {
		this.procurement = procurement;
	}

}
