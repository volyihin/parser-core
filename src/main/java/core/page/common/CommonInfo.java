package core.page.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import core.handler.BaseHandler;
import core.handler.HeaderType;
import core.handler.ProductHandler;
import core.handler.ProviderHandler;
import entity.PostAddress;
import entity.Procurement;
import entity.Product;

public class CommonInfo implements ICommonInfo {

	private final static Logger logger = Logger.getLogger(CommonInfo.class);

	private static final String COMMON_INFO_TABLE = "//*[@id='orderForm']/table/tbody/tr/td/table/tbody/tr[2]/td/div/table/tbody/tr/td/table/tbody/tr";
	private static final String CONTRACT_INFO_ID_XPATH = "//*[@id='orderForm']/table/tbody/tr/td/table/tbody/tr[1]/td/div/table/tbody/tr[4]/td/table/tbody/tr/td[1]/div/div/table/tbody/tr[2]/td[6]/a";
	private static final Pattern DATE_PATTERN = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}");

	private Procurement procurement;
	private List<Product> products = Lists.newArrayList();
	private HtmlUnitDriver page;
	private HeaderType headerType = HeaderType.base;
	private Map<String, String> values = new HashMap<String, String>();

	public CommonInfo(Procurement p) {
		super();
		this.procurement = p;
	}

	@Override
	public void start() {

		page = new HtmlUnitDriver();
		page.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);

		logger.info("Обрабатывается  закупка с URL " + procurement.getUrl());

		try {

			page.get(procurement.getUrl());

			if (page.getTitle().contains("регламентный")) {
				logger.info(page.getTitle());
				System.exit(0);
			}

			List<WebElement> trElements = page.findElements(By.xpath(COMMON_INFO_TABLE));

			List<WebElement> orderNumberSpan = page.findElements(By.xpath("//span[contains( text(),'Заказ №')]"));

			if (orderNumberSpan.size() > 0)
				procurement.setOrderNumber(orderNumberSpan.get(0).getText().replace("Заказ №", ""));

			List<WebElement> dates = page.findElements(By
					.xpath("//span[contains(text(), 'Опубликовано')]/following::span[1]"));

			if (dates.size() == 1) {
				String dateExpression = dates.get(0).getText();
				Matcher m = DATE_PATTERN.matcher(dateExpression);

				if (m.find())
					procurement.setDate(m.group());
			}

			if (trElements.size() > 0) {

				for (WebElement element : trElements) {
					BaseHandler bh = new BaseHandler(element);
					bh.execute();
					values.putAll(bh.getValues());
				}

				if (contractInfoExist()) {
					List<WebElement> tables = page.findElements(By.xpath("//table[@class='orderInfo']"));
					for (WebElement table : tables) {
						trElements = table.findElements(By.xpath("//tr"));

						for (WebElement element : trElements) {

							BaseHandler bh = new BaseHandler(element);
							bh.execute();
							values.putAll(bh.getValues());

							if (headerType == HeaderType.product) {
								ProductHandler productHandler = new ProductHandler(element);
								productHandler.execute();
								products.addAll(productHandler.getProducts());

								if (products.size() == 0)
									continue;

							}

							if (headerType == HeaderType.provider) {
								ProviderHandler providerHandler = new ProviderHandler(element);
								providerHandler.execute();
								procurement.setProvider(providerHandler.getProvider());
								procurement.setProviderInn(providerHandler.getProviderInn());

								if (procurement.getProvider() == null)
									continue;
							}

							headerType = defineHeader(element);
						}
					}
				}
			}

			fillProcuremt();

		} catch (Exception e) {
			logger.warn("Ошибка обработки закупки. Ссылка " + procurement.getUrl(), e);
		}
	}

	/**
	 * Включает флаг обрабоки как продуктов
	 * 
	 * @param element
	 * @return
	 */
	private HeaderType defineHeader(WebElement element) {
		if ("orderInfoHdr".equals(element.getAttribute("class"))) {
			if ((element.getText().contains("Предмет контракта")) || (element.getText().contains("Объект закупки")))
				return HeaderType.product;
			if (element.getText().contains("Информация о поставщиках"))
				return HeaderType.provider;
		}

		return HeaderType.base;
	}

	/**
	 * Проверка на сущестование "Сведения о контракте"
	 */
	private boolean contractInfoExist() {
		if (contractInfoElementExist() && contractCartPopupExist())
			return true;
		return false;
	}

	/**
	 * Проверка на сущестовование вкладки "Информация о контракте"
	 * 
	 * @return
	 */
	private boolean contractInfoElementExist() {

		String panelXPATH = "//td[@class='navigationLineElement']";
		try {
			List<WebElement> buttons = page.findElements(By.xpath(panelXPATH));
			if (buttons != null) {
				for (WebElement button : buttons) {
					if ((!Strings.isNullOrEmpty(button.getText()) && (button.getText()
							.contains("Информация о контракте")))) {
						WebElement a = button.findElement(By.xpath("a"));
						page.get(a.getAttribute("href"));
						return true;
					}

				}
			}
		} catch (NoSuchElementException e) {
			logger.warn("Текущая страница не содержит вкладки 'Информация о контракте'" + "xpath="
					+ CONTRACT_INFO_ID_XPATH);
		}
		return false;
	}

	/**
	 * Проверка на сущестовование ссылки в выпадающем меню "Карточка контракта"
	 * 
	 * @return
	 */
	private boolean contractCartPopupExist() {

		try {
			List<WebElement> spans = page.findElements(By.id("pgzform:menuPopup:menuItem2:out"));

			for (WebElement span : spans) {

				List<WebElement> aTags = span.findElements(By.xpath(".."));

				for (WebElement a : aTags) {
					page.get(a.getAttribute("href"));
					return true;
				}
			}
		} catch (Exception e) {
			logger.warn("Текущая страница не содержит вкладки 'Информация о контракте'");
		}
		return false;
	}

	public void fillProcuremt() {

		String customer = values.get("Заказчик");

		if (Strings.isNullOrEmpty(customer))
			customer = values.get("Закупку осуществляет");

		if ((!Strings.isNullOrEmpty(customer)) && (customer.split("\n").length == 2)) {
			customer = customer.split("\n")[1];
		}

		procurement.setCustomer(customer);

		String date = values.get("Дата заключения контракта");
		if ((!Strings.isNullOrEmpty(date)) && (Strings.isNullOrEmpty(procurement.getDate())))
			procurement.setDate(date);

		String endPrice = values.get("Цена контракта");
		if (!Strings.isNullOrEmpty(endPrice))
			procurement.setEndPrice(endPrice.replace(".", ","));

		String inn = values.get("ИНН Заказчика");
		if (Strings.isNullOrEmpty(inn))
			inn = values.get("ИНН");
		procurement.setInn(inn);

		if (Strings.isNullOrEmpty(procurement.getOrderNumber())) {
			String orderNumber = values.get("Номер заказа");
			procurement.setOrderNumber(orderNumber);
		}

		String orderName = values.get("Наименование заказа");
		if (Strings.isNullOrEmpty(orderName)) {
			orderName = values.get("Наименование объекта закупки");
			if (Strings.isNullOrEmpty(orderName))
				orderName = values.get("Полное наименование аукциона (предмет контракта)");
		}
		procurement.setOrderName(orderName);
		procurement.setProducts(products);

		String address = values.get("Адрес места нахождения");

		if (address != null)
			procurement.setPostAddress(PostAddress.fromString(address));
		procurement.setStartPrice(values.get("Начальная (Максимальная) цена контракта"));
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return Joiner.on("\n").withKeyValueSeparator("=").join(values);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.page.ICommonInfo#getProcurement()
	 */
	@Override
	public Procurement getProcurement() {
		return procurement;
	}

	public void setProcurement(Procurement procurement) {
		this.procurement = procurement;
	}

}
