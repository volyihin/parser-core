package core.page.common;

import entity.Procurement;

public class CommonInfoFactory {

	public static ICommonInfo getCommonInfo(Procurement pr) {

		String url = pr.getUrl();

		if (url.contains("/ea44/") || url.contains("/epz/"))
			return new CommonInfo44(pr);

		if (url.contains("/223/"))
			return new CommonInfo223(pr);

		return new CommonInfo(pr);
	}
}
