package core.page.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import core.handler.BaseHandler;
import entity.PostAddress;
import entity.Procurement;
import entity.Product;

public class CommonInfo223 implements ICommonInfo {
	private final static Logger logger = Logger.getLogger(CommonInfo223.class);

	private static final String COMMON_INFO_TABLE_223 = "/html/body/div[3]/font/div[2]/div[2]/table/tbody/tr";
	private static final String LIST_OF_LOTS_XPATH = "/html/body/div[3]/font/div[2]/div/ul/li[2]";
	private static final String LOTS_INFO_TABLE_XPATH = "/html/body/div[3]/font/div[2]/div[2]/table[2]/tbody/tr";
	private static final String PROTOCOLS_XPATH = "/html/body/div[3]/font/div[2]/div/ul/li[5]";

	private Procurement procurement;
	private List<Product> products = Lists.newArrayList();
	private WebDriver page;
	private boolean productExist = false;
	private Map<String, String> values = new HashMap<String, String>();

	public CommonInfo223(Procurement p) {
		super();
		this.procurement = p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.page.ICommonInfo#start()
	 */
	@Override
	public void start() {
		page = new HtmlUnitDriver(DesiredCapabilities.firefox());
		page.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);

		logger.info("Обрабатывается  закупка с URL " + procurement.getUrl());

		try {
			page.get(procurement.getUrl());

			if (page.getTitle().contains("регламентные работы")) {
				String message = "Ведуться регламентные работы";
				logger.error(message);
				JOptionPane.showMessageDialog(null, message);
				System.exit(0);
			}

			List<WebElement> trElements = page.findElements(By
					.xpath(COMMON_INFO_TABLE_223));

			if (trElements.size() > 0) {

				for (WebElement element : trElements) {
					BaseHandler bh = new BaseHandler(element);
					bh.execute();
					values.putAll(bh.getValues());
				}

				if (listOfLotsExist()) {
					List<WebElement> trels = page.findElements(By
							.xpath(LOTS_INFO_TABLE_XPATH));
					products = lotsToProducts(trels);
				}

				if (protocolsExist()) {
					List<WebElement> li = page
							.findElements(By
									.xpath("/html/body/div[3]/font/div[2]/div[2]/table/tbody/tr[2]/td[2]/div[2]/div/ul/li"));
					if (li.size() == 2) {
						List<WebElement> alinks = li.get(1).findElements(
								By.tagName("a"));
						if (alinks.size() == 1) {
							String url = alinks.get(0).getAttribute("onclick");
							if (!Strings.isNullOrEmpty(url)) {
								page.get("http://zakupki.gov.ru"
										+ extractFromBraces(url));

								List<WebElement> winner = page
										.findElements(By
												.xpath("//*[contains(text(),'Победитель')]/preceding-sibling::*[1]"));

								String provider = "";

								if (winner.size() == 1) {
									String winnerValue = winner.get(0)
											.getText();
									logger.debug("\n\n\nНашли победителя!!! \n\n "
											+ winnerValue);
									String[] values = winnerValue.split(",");
									for (String value : values) {
										if (value.split(":").length == 1) {
											provider = value;
											procurement.setProvider(provider);
										}
										if (value.contains("ИНН")) {
											procurement.setProviderInn(value
													.split(":")[1]);
										}
									}
								}

								if (!Strings.isNullOrEmpty(provider)) {
									List<WebElement> prices = page
											.findElements(By
													.xpath("//*[contains(text(),'"
															+ provider.trim()
															+ "')]/../following-sibling::*[2]"));

									if (prices.size() == 1) {
										String price = prices.get(0).getText();
										logger.debug("\n\n\nНашли цену!!! \n\n "
												+ price);
										String[] p2 = price.split(":");
										if (p2.length == 2)
											procurement.setEndPrice(p2[1]);

									}

								} else {
									List<WebElement> tds = page.findElements(By.xpath("*//td[contains(text(), 'Информация о поставщике, подавшем заявку:')]/following-sibling::*[1]"));
									if (tds.size() == 1) {
										provider = tds.get(0).getText();
										if (!Strings.isNullOrEmpty(provider)) {
											String[] data = Iterables.toArray(Splitter.on(",").trimResults().split(provider), String.class);
											if (data.length == 4) {
												procurement.setProvider(data[0]);
												procurement.setProviderInn(data[1]);
											}
										}
										
									}
										
								}
								
								
								
								
								
							}
						}
					}
				}
			}

			fillProcuremt();

		} catch (Exception e) {
			logger.warn("Ошибка обработки закупки. Ссылка " + procurement.getUrl(), e);
		}
	}

	private String extractFromBraces(String url) {
		int s = url.indexOf('(');
		int e = url.indexOf(')');
		if (s < e)
			url = url.substring(s + 2, e - 1);
		return url;
	}

	private boolean protocolsExist() {
		try {
			WebElement result = page.findElement(By.xpath(PROTOCOLS_XPATH));
			if (result != null) {
				if ((!Strings.isNullOrEmpty(result.getText()) && (result
						.getText().contains("Протоколы")))) {
					result.click();
					page.get(procurement.getUrl().replace("common-info",
							"protocols"));
					return true;
				}
			}
		} catch (NoSuchElementException e) {
			logger.warn("Текущая страница не содержит вкладки 'Протоколы'"
					+ "xpath=" + LIST_OF_LOTS_XPATH);
		}
		return false;
	}

	private List<Product> lotsToProducts(List<WebElement> trElements) {
		List<Product> products = Lists.newArrayList();
		for (WebElement tr : trElements) {
			List<WebElement> tdElements = tr.findElements(By.tagName("td"));
			if (tdElements.size() == 6)
				products.add(productFrom(tdElements));

		}

		return products;
	}

	private Product productFrom(List<WebElement> tdElements) {
		Product product = new Product();
		product.setName(tdElements.get(1).getText());
		product.setPrice(tdElements.get(3).getText());
		return product;
	}

	/**
	 * Проверка на сущестовование вкладки "Информация о контракте"
	 * 
	 * @return
	 */
	private boolean listOfLotsExist() {
		try {
			WebElement result = page.findElement(By.xpath(LIST_OF_LOTS_XPATH));
			if (result != null) {
				if ((!Strings.isNullOrEmpty(result.getText()) && (result
						.getText().contains("Список лотов")))) {
					result.click();
					page.get(procurement.getUrl().replace("common-info",
							"lot-list"));
					return true;
				}
			}
		} catch (NoSuchElementException e) {
			logger.warn("Текущая страница не содержит вкладки 'Список лотов'"
					+ "xpath=" + LIST_OF_LOTS_XPATH);
		}
		return false;
	}

	public void fillProcuremt() {

		procurement.setCustomer(Objects.firstNonNull(values.get("Заказчик"),
				values.get("Закупку осуществляет")));
		String innKpp = Objects.firstNonNull(values.get("ИНН \\ КПП"), "");
		String[] inn = innKpp.split("\\\\");
		procurement.setInn(inn[0]);
		procurement.setOrderNumber(values.get("Номер извещения"));
		procurement.setOrderName(Objects.firstNonNull(
				values.get("Наименование закупки"),
				values.get("Наименование объекта закупки")));

		String address = values.get("Адрес места нахождения");

		procurement.setProducts(products);

		if (address != null)
			procurement.setPostAddress(PostAddress.fromString(address));
		// procurement.setStartPrice(values.get("Начальная (Максимальная) цена контракта"));
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return Joiner.on("\n").withKeyValueSeparator("=").join(values);
	}

	public boolean isProductExist() {
		return productExist;
	}

	public void setProductExist(boolean productExist) {
		this.productExist = productExist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.page.ICommonInfo#getProcurement()
	 */
	@Override
	public Procurement getProcurement() {
		return procurement;
	}

	public void setProcurement(Procurement procurement) {
		this.procurement = procurement;
	}

}
