package core.page.iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.google.common.base.Strings;

import core.PageNotFound;
import core.handler.UrlEncoder;
import entity.Procurement;

public class IterablePage implements IIterablePage {

	private static final String ALL_NUMBER = "Всего записей: ";
	private static final Logger logger = Logger.getLogger(IterablePage.class);
	private static final String HREF = "href";
	private static final String PROCUREMENT_LINK_XPATH = "table/tbody/tr[1]/td[2]/dl/dt/a";
	private static final String TABLE_XPATH = "//*[@id='exceedSphinxPageSizeDiv']/div";
	private static final String ALL_NUMBER_XPATH = "//p[contains(text(), 'Всего записей')]";

	private String currentUrl;
	private Integer count;
	private WebDriver driver = new HtmlUnitDriver();
	private WebElement currentPage;
	private Integer pageNumber;
	private int end;
	private List<Procurement> ids = new ArrayList<Procurement>();

	public IterablePage(String url, int start) {
		super();

		driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
		this.pageNumber = start;
		this.currentUrl = UrlEncoder.setStartPage(start, url);
		driver.get(currentUrl);
		this.count = getCount(url.toString());
	}

	public void nextUrl() throws PageNotFound {
		String url = UrlEncoder.nextUrl(currentUrl);
		if (Strings.isNullOrEmpty(url))
			throw new PageNotFound("Не удалось получить страницу "
					+ currentUrl);
		currentUrl = url;
		pageNumber++;
		try {
			driver.get(currentUrl.toString());
		} catch (Throwable t) {
			new RuntimeException("Не удалось получить страницу " + currentUrl.toString());
		}

	}

	/**
	 * Всего закупок
	 * 
	 * @param url
	 * @return
	 */
	public Integer getCount(String url) {
		try {

			List<WebElement> e = driver
					.findElements(By.xpath(ALL_NUMBER_XPATH));
			String allNumberText = e.get(0).getText();
			if (!Strings.isNullOrEmpty(allNumberText)) {
				allNumberText = allNumberText.replace(ALL_NUMBER, "");
				return Integer.parseInt(allNumberText);
			} else {
				logger.warn("На странице " + url+ ". Не найден параметр 'Всего записей' xpath="
						+ ALL_NUMBER_XPATH);
			}
		} catch (NumberFormatException nfe) {
			logger.warn("Параметр 'Всего записей' не удалось преобразовать в число");
		} catch (Exception ex) {
			logger.warn("Не удалось получить параметр 'Всего записей'");
		}
		return 0;
	}

	@Override
	public boolean hasNext() {

		logger.info("Страница = " + currentUrl.toString());
		logger.info("Индекс страницы = " + pageNumber);

		try {

			if (pageNumber > end)
				return false;

			List<WebElement> divElements = driver.findElements(By
					.xpath(TABLE_XPATH));

			for (WebElement e : divElements) {
				WebElement el = e.findElement(By.xpath(PROCUREMENT_LINK_XPATH));
				List<WebElement> dateElements = e
						.findElements(By
								.xpath("table/tbody/tr[2]/td[2]/ul/li[contains(text(),'Опубликовано')]/following-sibling::li[1]"));
				List<WebElement> startPriceElements = e.findElements(By
						.xpath("table/tbody/tr/td[3]/dl"));
				String startPrice = "";
				if (startPriceElements.size() == 1)
					startPrice = startPriceElements.get(0).getText();
				String url = el.getAttribute(HREF);
				String date = "";
				if (dateElements.size() == 1)
					date = dateElements.get(0).getText();
				Procurement procurement = Procurement.fromUrl(url);
				if (!Strings.isNullOrEmpty(date))
					procurement.setDate(date);
				if (!Strings.isNullOrEmpty(startPrice))
					procurement.setStartPrice(startPrice);

				List<WebElement> orderNumbers = e
						.findElements(By
								.xpath("table/tbody/tr/td[2]/dl/dt/a[contains(text(), '№ ')]"));
				if (orderNumbers.size() == 1)
					procurement.setOrderNumber(orderNumbers.get(0).getText()
							.replaceAll("№ ", ""));

				procurement.setPageUrl(currentUrl.toString());

				ids.add(procurement);

			}

			logger.info("Найдено " + ids.size() + " закупок");

			if (ids.size() > 0) {
				return true;
			}

		} catch (Exception ex) {
			logger.warn("На старнице " + currentUrl.toString() + " нашли "
					+ ids, ex);
			logger.warn(ex);
			return false;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.page.IIterablePage#next()
	 */
	@Override
	public List<Procurement> next() {
		List<Procurement> copy = new ArrayList<Procurement>();
		copy.addAll(ids);
		ids = new ArrayList<Procurement>();
		return copy;
	}

	public void remove() {
		// TODO Auto-generated method stub

	}

	/**
	 * Проверка на валидность id закупки
	 * 
	 * @param id
	 * @return
	 */
	public boolean checkFZ(Integer id) {
		if ((id == 223) || (id == 44) || (id == 94))
			return false;
		return true;
	}

	@Override
	public Integer getPageNumber() {
		return pageNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.page.IIterablePage#getCount()
	 */
	@Override
	public Integer getCount() {
		return count;
	}

	public WebElement getCurrentPage() {
		return currentPage;
	}

	@Override
	public void setEnd(int end) {
		this.end = end;

	}

	@Override
	public String getCurrentUrl() {
		return currentUrl;
	}

}
