package core.page.iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.google.common.base.Strings;

import core.PageNotFound;
import core.handler.UrlEncoder;
import entity.Procurement;

public class OldIterablePage implements IIterablePage {

	private static final Logger logger = Logger.getLogger(OldIterablePage.class);

	private static final String ALL_NUMBER_XPATH = "//span[contains(text(),'Размещение завершено')]";

	private static final CharSequence ALL_NUMBER = "Размещение завершено ";

	private static final String TABLE_XPATH = "//table[@class='searchResultTable iceDatTbl']/tbody/tr";
	private String currentUrl;
	private Integer count;
	private WebDriver driver = new HtmlUnitDriver();
	private int end;
	private Integer pageNumber;
	private List<Procurement> ids = new ArrayList<Procurement>();

	public OldIterablePage(String url, int start) {

		driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
		this.pageNumber = start;
		this.currentUrl = UrlEncoder.setStartPage(start, url);
		this.count = getCount(url.toString());

		driver.get(currentUrl);
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean hasNext() {
		if (pageNumber > end)
			return false;

		try {
			List<WebElement> trElements = driver.findElements(By.xpath(TABLE_XPATH));

			for (WebElement e : trElements) {
				List<WebElement> tdElements = e.findElements(By.className("iceDatTblCol"));

				Procurement p = new Procurement();
				if (tdElements.size() == 0)
					break;

				String orderNumber = "";
				WebElement nameElement = tdElements.get(1);
				List<WebElement> aElements = nameElement.findElements(By.tagName("a"));
				for (WebElement el : aElements) {
					String href = el.getAttribute("href");
					if ((!Strings.isNullOrEmpty(href)) && ("iceOutLnk".equals(el.getAttribute("class")))) {
						p = Procurement.fromUrl(href);
						break;

					}

					if ((!Strings.isNullOrEmpty(href)) && (href.contains("/pgz/printForm?type=NOTIFICATION"))) {
						orderNumber = el.getText();
					}

				}

				if (tdElements.size() > 2)
					p.setDate(tdElements.get(2).getText());
				p.setOrderNumber(orderNumber);
				ids.add(p);

			}

			logger.info("Найдено " + ids.size() + " закупок");

		} catch (Exception ex) {
			logger.warn("На старнице " + getCurrentUrl().toString() + " нашли " + ids, ex);
			logger.warn(ex);
			return false;
		}
		if (ids.size() > 0) {
			return true;
		}

		return false;
	}

	public void nextUrl() throws PageNotFound {
		String url = UrlEncoder.nextUrl(currentUrl);
		if (Strings.isNullOrEmpty(url))
			throw new PageNotFound("Не удалось получить страницу " + currentUrl);
		currentUrl = url;
		pageNumber++;
		try {
			driver.get(currentUrl.toString());
		} catch (Throwable t) {
			new RuntimeException("Не удалось получить страницу " + currentUrl.toString());
		}

	}

	@Override
	public List<Procurement> next() {
		List<Procurement> copy = new ArrayList<Procurement>();
		copy.addAll(ids);
		ids = new ArrayList<Procurement>();
		return copy;
	}

	@Override
	public Integer getCount() {
		return count;
	}

	@Override
	public Integer getCount(String url) {
		try {

			List<WebElement> counts = driver.findElements(By.xpath("//td[@class='navigationLineSelectedElement']"));
			String result = "";
			for (WebElement e : counts) {
				String allNumberText = e.getText();
				if ((!Strings.isNullOrEmpty(allNumberText)) && (allNumberText.contains(ALL_NUMBER))) {
					allNumberText = allNumberText.replace(ALL_NUMBER, "");
					Matcher m = Pattern.compile("(\\d+)").matcher(allNumberText);
					if (m.find()) {
						result = m.group();
						return Integer.parseInt(result);
					}
				}
			}
		} catch (NumberFormatException nfe) {
			logger.warn("Параметр 'Всего записей' не удалось преобразовать в число");
		} catch (Exception ex) {
			logger.warn("Не удалось получить параметр 'Всего записей'", ex);
		}

		logger.warn("На странице " + url + ". Не найден параметр 'Всего записей' xpath=" + ALL_NUMBER_XPATH);

		return 0;
	}

	@Override
	public void setEnd(int end) {
		this.end = end;
	}

	public String getCurrentUrl() {
		return this.currentUrl;
	}

	public void setCurrentUrl(String currentUrl) {
		this.currentUrl = currentUrl;
	}

	@Override
	public Integer getPageNumber() {
		return this.pageNumber;
	}

}
