package core.page.iterator;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.google.common.base.Strings;

import core.url.UrlConstructer;

public class IterablePageResolver {

	private static final Logger logger = Logger.getLogger(IterablePageResolver.class);

	public static IIterablePage fromUrl(String url, int start) throws Exception {

		boolean newPage = resolve(url.toString());

		if (newPage)
			return new IterablePage(url, start);

		return new OldIterablePage(url, start);
	}

	/**
	 * Решает старый это сайт или новый, таким вот примитивным способом
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	private static boolean resolve(String url) throws Exception {

		if (!checkSiteAvailable(url))
			System.exit(0);

		Map<String, String> paramMap = UrlConstructer.getQueryMap(url);
		String pageNo = paramMap.get("pageNo");
		String index = paramMap.get("index");
		if (!Strings.isNullOrEmpty(pageNo))
			return true;

		if (!Strings.isNullOrEmpty(index))
			return false;
		
		return true;
	}

	/**
	 * Проверка на доступность сайта
	 * 
	 * @param url
	 * @return
	 */
	private static boolean checkSiteAvailable(String url) {
		WebDriver driver = new HtmlUnitDriver();
		driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
		driver.get(url.toString());
		if (driver.getTitle().contains("регламентные работы")) {
			logger.error(driver.getTitle());
			return false;
		}
		return true;
	}

}
