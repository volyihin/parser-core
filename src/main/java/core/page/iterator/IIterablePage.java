package core.page.iterator;

import java.util.Iterator;
import java.util.List;

import core.PageNotFound;
import entity.Procurement;

public interface IIterablePage extends Iterator<List<Procurement>> {

	/**
	 * Имеет ли станица закупки
	 */
	public boolean hasNext();

	/**
	 * Получает список незаполнненых закупок
	 */
	public List<Procurement> next();

	/**
	 * Возващает значение "Всего записей"
	 */
	public Integer getCount();

	Integer getCount(String url);

	public void setEnd(int end);
	
	public String getCurrentUrl();
	
	public Integer getPageNumber();
	
	public void nextUrl() throws PageNotFound;
	

}