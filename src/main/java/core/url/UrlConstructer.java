package core.url;

import java.util.LinkedHashMap;
import java.util.Map;

public class UrlConstructer {

	public static Map<String, String> getQueryMap(String query) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (String url : query.split("\\?")) {
			String[] params = url.split("&");
			for (String param : params) {
				String[] p = param.split("=");
				String name = p[0];
				String value = "";
				if (p.length == 2)
					value = p[1];
				map.put(name, value);

			}
		}

		return map;
	}
}
