<%@page import="entity.Procurement"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=windows-1251"
	pageEncoding="windows-1251"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>��������� ��������</title>

<link href="<c:url value="bootstrap.min.css"/>" rel="stylesheet">
</head>

<body>
	<div style="margin: 1%">
		<div style="display: inline;">
			<span class="label label-info">��������� </span> <span class="badge">${requestScope.current}
				�� ${requestScope.all}</span>
		</div>
		<br />
		<div style="display: inline;">
			<span class="label label-info">��������� ���������� �������� </span>
			<span class="badge">${requestScope.date}</span>
		</div>
		<br />
		<div style="display: inline;">
			<span class="label label-danger">��������� ��������� :</span>
			<p>${requestScope.message}</p>
		</div>
	</div>

	<form action="${pageContext.request.contextPath}/resultservlet" style="margin: 1%">
		<div class="form-group">
			<input type="submit" name="refreshBtn" value="��������" formmethod="get"
				class="btn btn-primary" />
		</div>
	</form>
	<form style="margin: 1%">


		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="heading">#</th>
					<th class="heading">ID</th>
					<th class="heading">����</th>
					<th class="heading">��������</th>
					<th class="heading">����� ������</th>
					<th class="heading">������������ ������</th>
					<th class="heading">��������� ����</th>
					<th class="heading">�������� ����</th>
					<th class="heading">���������</th>
					<th class="heading"></th>
					<th class="heading"></th>
					<th class="heading">��� ����������</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="p" items="${requestScope.procurements}"
					varStatus="loop">
					<tr>
						<td style="font-size: 10px;"><c:out value="${loop.index + 1}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.id}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.date}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.customer}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.orderNumber}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.orderName}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.startPrice}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.endPrice}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.provider}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.region}" /></td>
						<td style="font-size: 10px;"><a href="${p.url}">������ ��
								�������</a></td>
						<td style="font-size: 10px;"><c:out value="${p.inn}" /></td>
						<td style="font-size: 10px;"><c:out value="${p.providerInn}" /></td>
						<td style="font-size: 10px;"><a href="${p.pageUrl}">������
								�� �������� </a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form>
</body>
</html>