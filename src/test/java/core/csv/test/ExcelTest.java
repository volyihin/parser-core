package core.csv.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

import entity.PostAddress;
import entity.Procurement;
import entity.Product;
import excel.Excel;

public class ExcelTest {
	
	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void test1Excel() throws Exception {

		String fileName = "test" + Math.random() + ".xlsx";
		Excel ex = new Excel(fileName);
		List<Procurement> procurements = Lists.newArrayList();
		for (int i = 1; i <= 50; i++) {
			List<Product> products = new ArrayList<Product>();
			products.add(new Product(i + "_1"));
			products.add(new Product(i + "_2"));
			products.add(new Product(i + "_3"));
			products.add(new Product(i + "_4"));
			Procurement proc = new Procurement();
			proc.setOrderName("orderName" + "_" + i);
			proc.setCustomer("Customer" + "_" + i);
			proc.setDate("Date" + "_" + i);
			proc.setEndPrice("endPrice" + "_" + i);
			proc.setError(true);
			proc.setFo("FO" + "_" + i);
			proc.setInn("INN" + "_" + i);
			proc.setOrderNumber("orderNumber" + "_" + i);
			proc.setPostAddress(new PostAddress("_" + i));
			proc.setProvider("Provider" + "_" + i);
			proc.setProviderInn("ProviderInn" + "_" + i);
			proc.setRegion("region" + "_" + i);
			proc.setStartPrice("startPrice" + "_" + i);
			proc.setProducts(products);
			procurements.add(proc);
		}

		ex.writeToFile(procurements);
	}
}
