package core.csv.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.google.common.collect.Lists;

import core.page.iterator.IIterablePage;
import core.page.iterator.IterablePageResolver;
import entity.Procurement;

public class IterableTest {
	private static final String FZ44_FORM_URL = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&searchString=%D0%B8%D1%81%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5&placeOfSearch=FZ_44&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&okdpWithSubElements=false&orderStages=AF%2CCA&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false&law44.advantages[MP44]=I&law44.advantages[UG44]=I&law44.advantages[IN44]=I&law44.advantages[NO44]=I&law44.advantages[MPSP44]=I&law44.advantages[NOSP44]=I";

	private static final String NEW_FORM_URL = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2012&orderPublishDateTo=31.12.2013&okdpIds=131&okdpWithSubElements=true&districtIds=5277336&orderStages=PC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&law44.okpd.withSubElements=false";

	private static final String OLD_URL = "http://zakupki.gov.ru/pgz/public/action/search/extended/result?c0=true&a=true&c=FO&d=&_e=on&_f=on&_g=on&h=&p0=131&j=true&_j=on&k=&l=&m=&n=&o=&i=01.01.2012&p=22.07.2013&q=&r=&s=&b8=true&t=&customer.organizationId=&letter=%D0%90&u=5277336&_w=on&x=&y=&_z=on&a0=&sellerOrganizationId=&b7=false&f_NU44=c&f_NU=c&f_MC=c&f_IDT44=c&f_OLIMPSTROI=c&f_MP=c&f_ET44=c&f_TF44=c&f_3G44=c&f_TK44=c&f_TO44=c&f_TS44=c&b6=false&f_MPSP44=c&f_NO44=c&f_UG=c&f_UG44=c&f_NOSP44=c&f_MP44=c&f_IN=c&f_IN44=c&f_RM=c&b9=false&a1=&a2=&a4=&a5=&a6=&a7=&b5=&a8=&_a9=on&b10=false&_complaintSearchBlock.hasComplaint=on&complaintSearchBlock.complaintNumber=&complaintSearchBlock.subjectId=&complaintSearchBlock.subject=&complaintSearchBlock.controlOrganizationId=&complaintSearchBlock.controlOrganization=&b11=false&auditResultSearchBlock.auditNumber=&auditResultSearchBlock.subjectId=&auditResultSearchBlock.subject=&auditResultSearchBlock.controlOrganizationId=&auditResultSearchBlock.controlOrganization=&_auditResultSearchBlock.hasDecision=on&auditResultSearchBlock.decisionNumber=&auditResultSearchBlock.decisionDateFrom=&auditResultSearchBlock.decisionDateTo=&lotView=false&b0=&b1=true&_b1=on&b2=true&_b2=on&b3=true&_b3=on&b4=true&_b4=on&ext=cc71e1404ee742ee9934606213b73e0c&index=1&sortField=lastEventDate&descending=true&tabName=FO&pageX=&pageY=";
	
	@Test
	public void testCount() throws Exception {
		IIterablePage it = IterablePageResolver.fromUrl(NEW_FORM_URL, 1);
		it.setEnd(2);
		it.getCount();
	}
	
	@Test
	public void testNewForm() throws Exception {

		IIterablePage it = IterablePageResolver.fromUrl(NEW_FORM_URL, 1);
		it.setEnd(2);

		List<Procurement> all = Lists.newArrayList();
		System.out.println("�����:" + it.getCount());

		while (it.hasNext()) {
			List<Procurement> procurements = it.next();
			System.out.println(Procurement.orderNumberToString(procurements));
			System.out.println(procurements);
			all.addAll(procurements);
			it.nextUrl();
		}

		assertTrue(all.size() == 20);

	}

	@Test
	public void test44FzForm() throws Exception {

		IIterablePage it = IterablePageResolver.fromUrl(FZ44_FORM_URL, 1);
		it.setEnd(2);

		List<Procurement> all = Lists.newArrayList();
		System.out.println("�����:" + it.getCount());

		while (it.hasNext()) {
			List<Procurement> procurements = it.next();
			System.out.println(Procurement.orderNumberToString(procurements));
			System.out.println(procurements);
			all.addAll(procurements);
			it.nextUrl();
		}

		assertTrue(all.size() == 20);

	}

	@Test
	public void oldIterableTest() throws Exception {

		IIterablePage it = IterablePageResolver.fromUrl(OLD_URL, 1);
		it.setEnd(5);

		System.out.println("�����:" + it.getCount());

		while (it.hasNext()) {
			List<Procurement> p = it.next();
			System.out.println();
			System.out.println(Procurement.orderNumberToString(p));
			System.out.println(it.getCurrentUrl());
			it.nextUrl();
		}

	}

	@Test
	public void oldIterableTestPage1000To1003() throws Exception {

		IIterablePage it = IterablePageResolver.fromUrl(OLD_URL, 1000);
		it.setEnd(1003);

		System.out.println("�����:" + it.getCount());

		while (it.hasNext()) {
			System.out.println(it.next());
			it.nextUrl();
		}

	}

	@Test
	public void compareResults() throws Exception {
		String url = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=6&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2014&orderPublishDateTo=31.01.2014&okdpIds=35595%2C35596%2C35597%2C35598%2C35599%2C35600%2C35601%2C35602%2C35603%2C35604%2C35605%2C35606%2C35607%2C35608%2C35609%2C35610%2C35611%2C35590&okdpWithSubElements=true&orderStages=AF%2CCA%2CPC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false&";
		IIterablePage it = IterablePageResolver.fromUrl(url, 1);
		it.setEnd(1000);

		List<Procurement> one = Lists.newArrayList();
		while (it.hasNext()) {
			one.addAll(it.next());
			it.nextUrl();
		}

		it = IterablePageResolver.fromUrl(url, 1);
		it.setEnd(1000);

		List<Procurement> two = Lists.newArrayList();
		while (it.hasNext()) {
			two.addAll(it.next());
			it.nextUrl();
		}
		System.out.println("����������");
		two.removeAll(one);
		System.out.println(two.size());

	}

	/**
	 * (//a[(contains(@href,'javascript:goToPage'))])[1]
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String... args) throws Exception {

		// String testNewForm =
		// "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2014&orderPublishDateTo=31.01.2014&okdpIds=35595%2C35596%2C35597%2C35598%2C35599%2C35600%2C35601%2C35602%2C35603%2C35604%2C35605%2C35606%2C35607%2C35608%2C35609%2C35610%2C35611%2C35590&okdpWithSubElements=true&orderStages=AF%2CCA%2CPC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false&";
		// String xpath =
		// "//div[@class='paginator']//li[@class='currentPage']/following-sibling::li[1]//a";
		//
		// WebDriver driver = new HtmlUnitDriver(true);
		// driver.get(testNewForm);
		// WebElement nextPage = driver.findElement(By.xpath(xpath));
		//
		// nextPage.click();
		//
		// System.out.println(driver.getCurrentUrl());
		//
		//
		// IIterablePage it = IterablePageResolver.fromUrl(testNewForm, 1);
		// it.setEnd(1003);
		//
		// System.out.println("�����:" + it.getCount());
		// List<Procurement> ps = Lists.newArrayList();
		// while (it.hasNext()) {
		// ps.addAll(it.next());
		// // System.out.println(list(it.next()));
		// }
		// try {
		// Excel excel = new Excel(Math.random() + ".xlsx");
		// excel.writeToFile(ps);
		// } catch (IOException e) {
		// System.out.println("������ ������ � ����");
		// }
		//
		// System.out.println("UNIQ = " + Parser.getUniqs(ps));
		//
		// for (Procurement p1 : ps) {
		// int count = 0;
		// for (Procurement p2 : ps) {
		//
		// if (p1.getOrderNumber().equals(p2.getOrderNumber())) {
		// count++;
		// if (count > 1) {
		// System.out.println("\n\nNOT UNIQ "
		// + p1.getOrderNumber() + "\n"
		// + p2.getOrderNumber());
		// System.out.println(p1.getPageUrl());
		// System.out.println(p2.getPageUrl());
		// }
		// }
		// }
		// }

		WebDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_17);
		String testNewForm = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2014&orderPublishDateTo=31.01.2014&okdpIds=35595%2C35596%2C35597%2C35598%2C35599%2C35600%2C35601%2C35602%2C35603%2C35604%2C35605%2C35606%2C35607%2C35608%2C35609%2C35610%2C35611%2C35590&okdpWithSubElements=true&orderStages=AF%2CCA%2CPC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false&";
		String xpath = "//div[@class='paginator']//li[@class='currentPage']/following-sibling::li[1]//a";

		driver.get(testNewForm);
		WebElement nextPage = driver.findElement(By.xpath(xpath));

		nextPage.click();
		
		nextPage = driver.findElement(By.xpath(xpath));
		
		System.out.println(nextPage.getText());

		nextPage.click();
		
		System.out.println(nextPage.getText());

		System.out.println(driver.getCurrentUrl());

	}

	public static String list(List<Procurement> ps) {
		StringBuilder sb = new StringBuilder();
		for (Procurement p : ps) {
			sb.append(p.getOrderNumber()).append(",");
		}
		return sb.toString();
	}
}
