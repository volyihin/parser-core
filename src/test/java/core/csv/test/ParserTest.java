package core.csv.test;

import java.net.MalformedURLException;
import java.util.List;

import core.Parser;
import entity.Procurement;
import static org.junit.Assert.*;

public class ParserTest {

	private static String NEW_FORM_URL = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2012&orderPublishDateTo=31.12.2013&okdpIds=131&okdpWithSubElements=true&districtIds=5277336&orderStages=PC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&law44.okpd.withSubElements=false";
	private static final String FZ44_FORM_URL = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&searchString=%D0%B8%D1%81%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5&placeOfSearch=FZ_44&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&okdpWithSubElements=false&orderStages=AF%2CCA&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false&law44.advantages[MP44]=I&law44.advantages[UG44]=I&law44.advantages[IN44]=I&law44.advantages[NO44]=I&law44.advantages[MPSP44]=I&law44.advantages[NOSP44]=I";
	private static String OLD_URL = "http://zakupki.gov.ru/pgz/public/action/search/extended/result?c0=true&a=true&c=FO&d=&_e=on&_f=on&_g=on&h=&p0=131&j=true&_j=on&k=&l=&m=&n=&o=&i=01.01.2012&p=22.07.2013&q=&r=&s=&b8=true&t=&customer.organizationId=&letter=%D0%90&u=5277336&_w=on&x=&y=&_z=on&a0=&sellerOrganizationId=&b7=false&f_NU44=c&f_NU=c&f_MC=c&f_IDT44=c&f_OLIMPSTROI=c&f_MP=c&f_ET44=c&f_TF44=c&f_3G44=c&f_TK44=c&f_TO44=c&f_TS44=c&b6=false&f_MPSP44=c&f_NO44=c&f_UG=c&f_UG44=c&f_NOSP44=c&f_MP44=c&f_IN=c&f_IN44=c&f_RM=c&b9=false&a1=&a2=&a4=&a5=&a6=&a7=&b5=&a8=&_a9=on&b10=false&_complaintSearchBlock.hasComplaint=on&complaintSearchBlock.complaintNumber=&complaintSearchBlock.subjectId=&complaintSearchBlock.subject=&complaintSearchBlock.controlOrganizationId=&complaintSearchBlock.controlOrganization=&b11=false&auditResultSearchBlock.auditNumber=&auditResultSearchBlock.subjectId=&auditResultSearchBlock.subject=&auditResultSearchBlock.controlOrganizationId=&auditResultSearchBlock.controlOrganization=&_auditResultSearchBlock.hasDecision=on&auditResultSearchBlock.decisionNumber=&auditResultSearchBlock.decisionDateFrom=&auditResultSearchBlock.decisionDateTo=&lotView=false&b0=&b1=true&_b1=on&b2=true&_b2=on&b3=true&_b3=on&b4=true&_b4=on&ext=cc71e1404ee742ee9934606213b73e0c&index=1&sortField=lastEventDate&descending=true&tabName=FO&pageX=&pageY=";
	//private static String NEW_FORM_223 = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.02.2014&orderPublishDateTo=28.02.2014&okdpIds=35595%2C35597%2C35598%2C35599%2C35600%2C35601%2C35602%2C35603%2C35607%2C35608%2C35609%2C35590&okdpWithSubElements=true&orderStages=AF%2CCA%2CPC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false";

	public void testNewForm() throws MalformedURLException {
		Parser parser = new Parser(NEW_FORM_URL, 1, 3);
		parser.disableInputParam();
		parser.disableWriteToFile();
		parser.start();

		List<Procurement> procurements = parser.getProcurements();
		assertTrue(procurements.size() == 30);
	}



	public void testNewForm44FZ() throws MalformedURLException {
		Parser parser = new Parser(FZ44_FORM_URL, 1, 3);
		parser.disableInputParam();
		parser.start();

		List<Procurement> procurements = parser.getProcurements();
		assertTrue(procurements.size() == 30);

	}

	public static void testOldForm() throws MalformedURLException {
		Parser parser = new Parser(OLD_URL, 1, 2);
		parser.disableInputParam();
		parser.start();

		List<Procurement> procurements = parser.getProcurements();
		assertTrue(procurements.size() == 20);

	}

	public static void testNewFormEmpty() throws MalformedURLException {
		String url = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2014&orderPublishDateTo=31.01.2014&okdpIds=1571%2C1572%2C35590%2C35595%2C35596%2C35597%2C35598%2C35599%2C35600%2C35601%2C35602%2C35603%2C35604%2C35605%2C35606%2C35607%2C35608%2C35609%2C35610%2C35611&okdpWithSubElements=true&orderStages=AF%2CCA%2CPC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false";
		Parser parser = new Parser(url, 1, 1000);
		parser.disableInputParam();
		parser.start();

	}

	public static void testNewForm217() throws MalformedURLException {
		String url = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=6&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2014&orderPublishDateTo=31.01.2014&okdpIds=35595%2C35596%2C35597%2C35598%2C35599%2C35600%2C35601%2C35602%2C35603%2C35604%2C35605%2C35606%2C35607%2C35608%2C35609%2C35610%2C35611%2C35590&okdpWithSubElements=true&orderStages=AF%2CCA%2CPC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false&";
		Parser parser = new Parser(url, 1, 20);
		parser.disableInputParam();
		parser.start();

	}
	
	public static void testNewForm13() throws MalformedURLException {
		String url = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_94&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2013&orderPublishDateTo=31.12.2013&okdpWithSubElements=false&regionIds=5277373&orderStages=PC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&contractPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWord94=%D1%85%D0%BB%D0%BE%D1%80%D0%B8%D0%B4+%D0%BD%D0%B0%D1%82%D1%80%D0%B8%D1%8F&matchingWordPlace94=NOTIFICATIONS%2CATTACHMENTS%2CEXPLANATIONS%2CPROTOCOLS&changeParameters=true&showLotsInfo=false&law44.okpd.withSubElements=false";
		Parser parser = new Parser(url, 1, 20);
		parser.disableInputParam();
		parser.start();

	}
	
	public static void test5() throws MalformedURLException {
		String url = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?placeOfSearch=FZ_44&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2013&orderPublishDateTo=31.12.2014&regionIds=5277373&orderStages=PC&law44.advantages[MP44]=I&law44.advantages[UG44]=I&law44.advantages[IN44]=I&law44.advantages[NO44]=I&law44.advantages[MPSP44]=I&law44.advantages[NOSP44]=I&searchString=%D1%85%D0%BB%D0%BE%D1%80%D0%B8%D0%B4+%D0%BD%D0%B0%D1%82%D1%80%D0%B8%D1%8F&morphology=false&strictEqual=false";
		Parser parser = new Parser(url, 1, 20);
		parser.disableInputParam();
		parser.start();

	}
	 
	
	public static void main(String... args) throws MalformedURLException {
		test5();
	}
}
