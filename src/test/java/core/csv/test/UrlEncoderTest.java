package core.csv.test;

import org.junit.Assert;
import org.junit.Test;

import core.handler.UrlEncoder;

public class UrlEncoderTest {
	private static String NEW_FORM_URL = "http://zakupki.gov.ru/epz/order/extendedsearch/search.html?sortDirection=false&sortBy=UPDATE_DATE&recordsPerPage=_10&pageNo=1&placeOfSearch=FZ_223&searchType=ORDERS&morphology=false&strictEqual=false&orderPriceCurrencyId=-1&orderPublishDateFrom=01.01.2012&orderPublishDateTo=31.12.2013&okdpIds=131&okdpWithSubElements=true&districtIds=5277336&orderStages=PC&headAgencyWithSubElements=false&smallBusinessSubject=I&rnpData=I&executionRequirement=I&penalSystemAdvantage=I&disabilityOrganizationsAdvantage=I&russianGoodsPreferences=I&orderPriceCurrencyId=-1&okvedWithSubElements=false&jointPurchase=false&byRepresentativeCreated=false&selectedMatchingWordPlace223=NOTICE_AND_DOCS&matchingWordPlace94=NOTIFICATIONS&changeParameters=true&law44.okpd.withSubElements=false";

	// private static String OLD_URL =
	// "http://zakupki.gov.ru/pgz/public/action/search/extended/result?c0=true&a=true&c=FO&d=&_e=on&_f=on&_g=on&h=&p0=131&j=true&_j=on&k=&l=&m=&n=&o=&i=01.01.2012&p=22.07.2013&q=&r=&s=&b8=true&t=&customer.organizationId=&letter=%D0%90&u=5277336&_w=on&x=&y=&_z=on&a0=&sellerOrganizationId=&b7=false&f_NU44=c&f_NU=c&f_MC=c&f_IDT44=c&f_OLIMPSTROI=c&f_MP=c&f_ET44=c&f_TF44=c&f_3G44=c&f_TK44=c&f_TO44=c&f_TS44=c&b6=false&f_MPSP44=c&f_NO44=c&f_UG=c&f_UG44=c&f_NOSP44=c&f_MP44=c&f_IN=c&f_IN44=c&f_RM=c&b9=false&a1=&a2=&a4=&a5=&a6=&a7=&b5=&a8=&_a9=on&b10=false&_complaintSearchBlock.hasComplaint=on&complaintSearchBlock.complaintNumber=&complaintSearchBlock.subjectId=&complaintSearchBlock.subject=&complaintSearchBlock.controlOrganizationId=&complaintSearchBlock.controlOrganization=&b11=false&auditResultSearchBlock.auditNumber=&auditResultSearchBlock.subjectId=&auditResultSearchBlock.subject=&auditResultSearchBlock.controlOrganizationId=&auditResultSearchBlock.controlOrganization=&_auditResultSearchBlock.hasDecision=on&auditResultSearchBlock.decisionNumber=&auditResultSearchBlock.decisionDateFrom=&auditResultSearchBlock.decisionDateTo=&lotView=false&b0=&b1=true&_b1=on&b2=true&_b2=on&b3=true&_b3=on&b4=true&_b4=on&ext=cc71e1404ee742ee9934606213b73e0c&index=1&sortField=lastEventDate&descending=true&tabName=FO&pageX=&pageY=";

	@Test
	public void testNewForm() {
		String newFormPage2 = UrlEncoder.nextUrl(NEW_FORM_URL);
		Assert.assertTrue(UrlEncoder.getParameter("pageNo", newFormPage2).equals("2"));
		String newFormPage3 = UrlEncoder.nextUrl(newFormPage2);
		Assert.assertTrue(UrlEncoder.getParameter("pageNo", newFormPage3).equals("3"));

	}

}
