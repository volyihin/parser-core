package core.csv.test;

import static org.junit.Assert.*;
import org.junit.Test;

import core.page.common.CommonInfoFactory;
import core.page.common.ICommonInfo;
import entity.Procurement;

public class CommonInfo44Test {

	@Test
	public void start18720() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?noticeId=18720");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		
		System.out.println(p);		
		assertTrue(p.getCustomer() != null);
		assertTrue(p.getOrderName() != null);
		assertTrue(p.getDate() != null);
		assertTrue(p.getProducts().size() == 1);

	}

	@Test
	public void start0348200044214000001() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/epz/order/notice/zk44/view/common-info.html?regNumber=0348200044214000001");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		
		System.out.println(p);		
		assertTrue(p.getCustomer() != null);
		assertTrue(p.getPostAddress() != null);
		assertTrue(p.getOrderName() != null);
		assertTrue(p.getProducts().size() == 1);

	}
	
}
