package core.csv.test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import core.page.common.CommonInfoFactory;
import core.page.common.ICommonInfo;
import entity.Procurement;

public class CommonInfoTest {

	private static final Logger logger = Logger.getLogger(CommonInfoTest.class);

	@Test
	public void start3074245() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/pgz/public/action/orders/info/common_info/show?source=epz&notificationId=3074245");

		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		logger.info(commonInfo.getProcurement());

	}

	@Test
	public void start535691() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/223/purchase/public/purchase/info/common-info.html?noticeId=535691&epz=true");

		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();

	}

	@Test
	public void start223() {

		Procurement p = Procurement
				.fromUrl("	http://zakupki.gov.ru/223/purchase/public/purchase/info/common-info.html?noticeId=766520&epz=true");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();

	}

	@Test
	public void start1Reference() {

		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/pgz/public/action/orders/info/common_info/show?notificationId=6404525");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		System.out.println(p);
		assertNotNull(p.getCustomer());
		assertNotNull(p.getDate());
		assertNotNull(p.getInn());
		assertNotNull(p.getOrderName());
		assertNotNull(p.getOrderNumber());
		assertNotNull(p.getProvider());
		assertNotNull(p.getProviderInn());
		assertNotNull(p.getProducts().get(0));
		assertNotNull(p.getUrl());
	}

	@Test
	public void start3Reference() {

		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/pgz/public/action/orders/info/common_info/show?source=epz&notificationId=8275854");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		p = commonInfo.getProcurement();
		System.out.println(p);
		assertNotNull(p.getCustomer());
		assertNotNull(p.getDate());
		assertNotNull(p.getInn());
		assertNotNull(p.getOrderName());
		assertNotNull(p.getOrderNumber());
		assertNotNull(p.getProvider());
		assertNotNull(p.getProviderInn());
		assertNotNull(p.getProducts().get(0));
		assertNotNull(p.getUrl());

	}

	@Test
	public void encode() {
		try {
			String result = URLDecoder.decode(
					"%D0%B3%D0%B5%D0%BD%D0%B5%D1%80%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9+%D0%BF%D0%BB%D0%B0%D0%BD",
					"UTF-8");
			System.out.println(result.replaceAll(" ", "_"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
