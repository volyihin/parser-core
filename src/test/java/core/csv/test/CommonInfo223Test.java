package core.csv.test;

import static org.junit.Assert.*;
import org.junit.Test;

import core.page.common.CommonInfoFactory;
import core.page.common.ICommonInfo;
import entity.Procurement;

public class CommonInfo223Test {
	
	@Test
	public void start811840() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/223/purchase/public/purchase/info/common-info.html?noticeId=811840&epz=true");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		System.out.println(commonInfo.getProcurement());
		
		assertTrue(p.getEndPrice() != null);
		assertTrue(p.getProvider() != null);
		assertTrue(p.getProviderInn() != null);
	}
	
	@Test
	public void start916847() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/223/purchase/public/purchase/info/common-info.html?noticeId=916847&epz=true");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		System.out.println(commonInfo.getProcurement());
		
		assertTrue(p != null);
		assertTrue(p.getCustomer() != null);
	}
	
	@Test
	public void start1535815() {
		Procurement p = Procurement
				.fromUrl("http://zakupki.gov.ru/223/purchase/public/purchase/info/common-info.html?lotId=1535815&purchaseId=1025074&purchaseMethodType=EP&epz=true");
		ICommonInfo commonInfo = CommonInfoFactory.getCommonInfo(p);
		commonInfo.start();
		System.out.println(commonInfo.getProcurement());
		
		assertTrue(p != null);
		assertTrue(p.getCustomer() != null);
	}
	
	
}
